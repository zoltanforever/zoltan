﻿using UnityEngine;

public class Highscore : MonoBehaviour {

    TMPro.TextMeshPro _textMesh;

    public static readonly string NAME = "HighScore";

    static readonly string TEXT = "Highscore: ";
    int _highscore = 0;
    
	void Start () {
        _textMesh = GetComponent<TMPro.TextMeshPro>();
        name = NAME;
	}

    void Update()
    {
        _textMesh.text = TEXT + _highscore;
    }
	
	public int MyHighscore
    {
        get { return _highscore; }
        set { _highscore = value; }
    }
}
