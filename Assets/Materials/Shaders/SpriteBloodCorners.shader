Shader "Zoltan/SpriteBloodCorners"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		_Health("Health", Range(0, 1)) = 0
		_Frequency("Frequency", Float) = 50
		_Amplitude("Amplitude", Float) = 0.001
		_Speed("Speed", Float) = 20
		_RednessBlend("Redness Blend", Range(0,1)) = 0.3
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite On
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float2 texcoord  : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
			};
			

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _AlphaTex;
			fixed4 _Color;
			float _RednessBlend = 0.3;

			float _Health;
			static float2 _Center = float2(0.5, 0.5);
			static float _MaxOffset = 0.4;
			float _Frequency;
			float _Amplitude;
			float _Speed;


			fixed4 SampleSpriteTexture (float2 uv)
			{
				fixed4 color = tex2D (_MainTex, uv);

#if ETC1_EXTERNAL_ALPHA
				// get the color from an external texture (usecase: Alpha support for ETC1 on android)
				color.a = tex2D (_AlphaTex, uv).r;
#endif //ETC1_EXTERNAL_ALPHA

				return color;
			}

			float2 rotate90(float2 v)
			{
				return float2( -((_Center.y - v.y)) + _Center.x, -((v.x - _Center.x)) + _Center.y);
			}

			fixed4 getBloodColor (float2 uv, float distToCenter)
			{
				float2 newUV = uv;
				float2 dirToCenter = (_Center - uv);
				newUV += dirToCenter * distToCenter * _Health * _MaxOffset;
				newUV = newUV + (sin(_Health * _Speed + rotate90(newUV) * _Frequency) * _Amplitude); 
				return SampleSpriteTexture(newUV);
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				float distToCenter = distance(IN.texcoord, _Center) / 0.707;
				fixed4 bloodCol = getBloodColor(IN.texcoord, distToCenter);
				bloodCol.rgb *= bloodCol.a;

				float blendCoefficient = _RednessBlend * (1 - _Health) * distToCenter;
				bloodCol = (1 - blendCoefficient) * bloodCol + _Color * blendCoefficient;			

				return bloodCol;
			}
		ENDCG
		}
	}
}
