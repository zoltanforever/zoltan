﻿using UnityEngine;
using System.Collections;

public class SpiralingMovement : BaseMovement {

    public float _angularSpeedInDegrees = 30;

	Vector3 _vectorToPlayer;
    float _spiralingDirection = -1; // 1 or -1 for right or left
    float _currentAngle;


    protected override void Initialize()
    {
        _spiralingDirection = Random.value > 0.5f ? 1 : -1;
		UpdateVectorToPlayer ();
        _currentAngle = Mathf.Atan2(_vectorToPlayer.z, _vectorToPlayer.x);
    }

    protected override void UpdateMovement()
    {
        base.UpdateMovement();
		UpdateVectorToPlayer ();
        _currentAngle += _angularSpeedInDegrees * _spiralingDirection * Time.deltaTime * Mathf.Deg2Rad;

		float mag = new Vector3(_vectorToPlayer.x, 0, _vectorToPlayer.z).magnitude;
		gameObject.transform.position = new Vector3(mag * Mathf.Cos(_currentAngle), gameObject.transform.position.y, mag * Mathf.Sin(_currentAngle));
    }

	void UpdateVectorToPlayer()
	{
		_vectorToPlayer = gameObject.transform.position - _player.transform.position;
	}
}
