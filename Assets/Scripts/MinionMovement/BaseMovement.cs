﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BaseMovement : MonoBehaviour {

    public float _moveSpeed = 1;
    public float _moveRange = 2f;

    protected GameObject _player;

    protected BaseMinion _minion;

	bool _isAnimatingIn = true;

    // Use this for initialization
    void Start () {
        _player = GameObject.FindGameObjectWithTag("MainCamera");
        _minion = transform.GetComponentInChildren<BaseMinion>();
        _minion.DieEvent += Destroy;
		Initialize();
		AnimateIn ();       
    }
	
	// Update is called once per frame
	void Update () {
	    if (!_isAnimatingIn && (transform.position - _player.transform.position).magnitude > _moveRange)
        {
            _minion.isMoving = true;
            UpdateMovement();
        } else
        {
            _minion.isMoving = false;
        }
	}

    virtual protected void Initialize()
    {
    }

    virtual protected void UpdateMovement()
    {
        Vector3 newPosition = Vector3.MoveTowards(transform.position, _player.transform.position, _moveSpeed * Time.deltaTime);
        newPosition.y = transform.position.y;
        transform.position = newPosition;

        LookAtPlayer();
    }

	virtual protected void AnimateIn()
	{
		LookAtPlayer();
		float y = transform.position.y;
		Sequence s = DOTween.Sequence();
		transform.position = new Vector3(transform.position.x, -2, transform.position.z);
		s.Append(transform.DOMoveY(y + 1f, 0.6f));
		s.Append(transform.DOMoveY(y, 0.3f));
		s.OnComplete(() => { _isAnimatingIn = false; });
	}

    protected void LookAtPlayer()
    {
        var lookDir = _player.transform.position - transform.position;
        lookDir.y = 0; // keep only the horizontal direction
        transform.rotation = Quaternion.LookRotation(lookDir);
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }

}
