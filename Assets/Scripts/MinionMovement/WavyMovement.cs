﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class WavyMovement : BaseMovement
{

    public float _waveAmplitude = 3f;
    public float _wavePeriodInSeconds = 2f;

    float _currentAmplitude;

    Vector3 _linearPosition;

    protected override void Initialize()
    {
        _linearPosition = transform.position;

        Sequence s = DOTween.Sequence();
        _currentAmplitude = -_waveAmplitude;
        s.Append(DOTween.To(GetCurrentAmplitude, SetCurrentAmplitude, _waveAmplitude, _wavePeriodInSeconds * 0.5f));
        s.Append(DOTween.To(GetCurrentAmplitude, SetCurrentAmplitude, -_waveAmplitude, _wavePeriodInSeconds * 0.5f));
        s.SetLoops(-1);
    }

    protected override void UpdateMovement()
    {
        _linearPosition = Vector3.MoveTowards(_linearPosition, _player.transform.position, _moveSpeed * Time.deltaTime);
        _linearPosition.y = transform.position.y;

        transform.position = _linearPosition + transform.right * _currentAmplitude;

        LookAtPlayer();           
    }

    float GetCurrentAmplitude()
    {
        return _currentAmplitude;
    }

    void SetCurrentAmplitude(float a)
    {
        _currentAmplitude = a;
    }

}
