﻿using UnityEngine;
using System.Collections;

public class BulletListener : MonoBehaviour {

    public event GotShotEventHandler BulletHit;

    void OnCollisionEnter(Collision c)
    {
        for (int i = 0; i < c.contacts.Length; i++)
        {

            if (c.contacts[i].otherCollider.tag == "MinionBullet")
            {
                GameObject bullet = c.contacts[i].otherCollider.gameObject;
                if (BulletHit != null)
					BulletHit(bullet.GetComponent<Projectile>().Damage, bullet.transform);
                Destroy(bullet.gameObject);
            }

        }
    }
}
