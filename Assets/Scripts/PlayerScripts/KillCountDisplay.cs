﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KillCountDisplay : MonoBehaviour {
    
    TextMesh _text;
    
    void Start () {
        _text = transform.FindChild("Text").GetComponent<TextMesh>();
	}

    public void DisplayKills(int kills)
    {
        _text.text = kills.ToString();
    }
}
