﻿using System;
using UnityEngine;

public class PlayerDodgeInput : MonoBehaviour {

  

    public float _dodgeCooldownInSeconds = 0.3f;
    public float _dodgeAtAcceleration = 0.5f;

    DodgeBehaviour _dodgeBehaviour;
    float _lastDodgeTime;
    Vector3 _lastDodgeUp;
    bool _isDodgeAllowed = true;
    const float GRAVITATIONAL_ACCELERATION = -1;

    void Start()
    {
        _dodgeBehaviour = GetComponent<DodgeBehaviour>();
        _dodgeBehaviour.SetDodgeDurationInSeconds(_dodgeCooldownInSeconds);

        AutoSpawnerController msc = GameObject.FindObjectOfType<AutoSpawnerController>();
        msc.WaveStageEnded += DoNotAllowDodge;
        msc.WaveEnded += DoNotAllowDodge;

        GunController gunController = GameObject.FindObjectOfType<GunController>();
        gunController.GunSelected += AllowDodge;
    }

    private void AllowDodge()
    {
        _isDodgeAllowed = true;
        _dodgeBehaviour.ResumeDodge();
    }

    private void DoNotAllowDodge()
    {
        _isDodgeAllowed = false;
        _dodgeBehaviour.PauseDodge();
    }

    void Update()
    {
        if (Time.time - _lastDodgeTime > _dodgeCooldownInSeconds)
        {
            if (ShouldDodgeDown())
            {
                _lastDodgeTime = Time.time;
                _dodgeBehaviour.Dodge(Vector3.down);
            }
        }     
    }

    bool ShouldDodgeDown()
    {
        if (!_isDodgeAllowed)
        {
            return false;
        }

        if (Application.isEditor)
        {
            return Input.GetKeyDown(KeyCode.S);
        }
        else
        {
            float maxOtherAccelerations = 0.3f;
            bool toReturn = (Input.acceleration.y > GRAVITATIONAL_ACCELERATION + _dodgeAtAcceleration &&
                Input.acceleration.y < 0 &&
                Mathf.Abs(Input.acceleration.x) < maxOtherAccelerations &&
                Mathf.Abs(Input.acceleration.z) < maxOtherAccelerations);
            return toReturn;
        }
    }
}
