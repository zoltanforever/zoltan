﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ZoltanWarriorController : MonoBehaviour {

    public event HealthStatusHandler DeathEvent;

    public GameObject _gunControllerGo;
    GunController _gunController;

    public GameObject _deathRigGo;
    DeathRig _deathRig;

    public GameObject _playerBodyGo;

    public GameObject _healthStatusGo;
    HealthStatus _healthStatus;

    public GameObject _killCountDisplayGo;
    KillCountDisplay _killCountDisplay;

    public GameObject _bulletCountGo;
    BulletCountDisplay _bulletCount;

    AudioController _audioController;

    MainMenuController _mainMenu;

    public GameObject _gunMenuGo;
    GunMenuController _gunMenuController;
   
    int _killCount;
    int _lastKillCount;
    float _timeSlowdown = 0.05f;

    Transform _lastImpact;

    void Awake()
    {
        _gunControllerGo = (GameObject)Instantiate(_gunControllerGo, transform);
        _gunControllerGo.transform.localPosition = new Vector3(-0.03f, -0.29f, 0.35f);
        _gunController = _gunControllerGo.GetComponent<GunController>();
        _gunController.GunSelected += Resume;

        _healthStatusGo = (GameObject)Instantiate(_healthStatusGo, transform);
        _healthStatus = _healthStatusGo.GetComponent<HealthStatus>();
        _healthStatus.DyingEvent += OnDying;

        _playerBodyGo = (GameObject)Instantiate(_playerBodyGo, transform);
        _playerBodyGo.transform.localPosition = Vector3.zero;
        _playerBodyGo.GetComponent<BulletListener>().BulletHit += TakeDamage;

        _audioController = GetComponent<AudioController>();

        _killCountDisplayGo = (GameObject)Instantiate(_killCountDisplayGo, transform);
        _killCountDisplayGo.transform.localPosition = new Vector3(-0.4f, -0.45f, 0.8f);
        _killCountDisplay = _killCountDisplayGo.GetComponent<KillCountDisplay>();

        _bulletCountGo = (GameObject)Instantiate(_bulletCountGo, transform);
        _bulletCountGo.transform.localPosition = new Vector3(-0.4f, -0.27f, 0.8f);
        _bulletCount = _bulletCountGo.GetComponent<BulletCountDisplay>();
        _gunController.SetBulletDisplay(_bulletCount);

        _gunMenuController = new GunMenuController(_gunController, transform, _gunMenuGo);
        _gunMenuController.Initialize();

        AutoSpawnerController msc = GameObject.FindObjectOfType<AutoSpawnerController>();
        msc.Initialize();
        msc.WaveStageEnded += EnterGunpickPhase;
        msc.WaveEnded += EnterGunpickPhase;
        msc.StartWaves();

        _deathRigGo = (GameObject)Instantiate(_deathRigGo);
        _deathRig = _deathRigGo.GetComponent<DeathRig>();

         MainMenuController _mainMenuController = new GameObject().AddComponent<MainMenuController>();
        _mainMenuController.SetZoltanWarriorController(this);
      
    }
    
	public void TakeDamage(int damage, Transform impact)
    {
		_healthStatus.TakeDamage(damage);
        _audioController.PlayTakeDamageSound();
        _lastImpact = impact;
    }
	
	void Update ()
    {
        _gunMenuController.Update();
    }


    private void EnterGunpickPhase()
    {
        Time.timeScale = _timeSlowdown;
        _lastKillCount = _killCount;

        StartCoroutine(_gunMenuController.ActiveGunMenuAfter(0.05f));

        _gunController.Pause();
    }

    void Resume()
    {
        StartCoroutine(ResumeNextFrame());
    }

    IEnumerator ResumeNextFrame()
    {
        yield return new WaitForEndOfFrame();

        Time.timeScale = 1.0f;
        _gunMenuController.SetGunMenuActive(false);
        _gunController.Unpause();
    }

    public void IncrementKillCount(){
        _killCount++;
        _killCountDisplay.DisplayKills(_killCount);

        if(Random.Range(0, 5) > 2)
            _audioController.PlayKillSound();
    }

    public int GetKillCount()
    {
        return _killCount;
    }

    readonly float IMPACT_FORCE = 30f;
    void OnDying()
    { 
        GameObject.Destroy(_playerBodyGo.GetComponent<Collider>());
        GameObject.Destroy(_playerBodyGo.GetComponent<Rigidbody>());

        Vector3 force;
        if (!_lastImpact)
        {
            force = new Vector3(-1, 0, 0) * IMPACT_FORCE;
        }
        else
        {
            force = _lastImpact.transform.forward * IMPACT_FORCE;
        }

        _deathRig.ReparentAndAddForce(transform, force);

        _gunController.gameObject.SetActive(false);
        StartCoroutine(DeadAfterDelay());
    }

    readonly float DEATH_DELAY = 2;
    IEnumerator DeadAfterDelay()
    {
        yield return new WaitForSeconds(DEATH_DELAY);
        if (DeathEvent != null)
        {
            DeathEvent();
        }
    }
}
