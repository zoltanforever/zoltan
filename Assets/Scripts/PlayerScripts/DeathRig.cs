﻿using UnityEngine;

public class DeathRig : MonoBehaviour {

    public Rigidbody Body;
    public GameObject Head;

    // Use this for initialization
    void Start () {
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.P))
        {
            Impulse();
        }
	}

    void Impulse()
    {
        Body.AddForce(new Vector3(-1, 0, 0) * 20f);
    }

    public void ReparentAndAddForce(Transform player, Vector3 force)
    {
        gameObject.SetActive(true);
        player.SetParent(Body.transform);
        Body.AddForceAtPosition(force, Head.transform.position);
    }
}
