﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCountDisplay : MonoBehaviour
{

    TextMesh _text;
    int _currentCount = 0;

    void Start()
    {
        _text = transform.FindChild("Text").GetComponent<TextMesh>();
        DisplayBullets(_currentCount);
    }

    public void DisplayBullets(int bullets)
    {
        _currentCount = bullets;

        if(_text != null)
            _text.text = _currentCount.ToString();
    }
}