﻿using UnityEngine;
using DG.Tweening;

public class DodgeBehaviour:MonoBehaviour  {

    public float _upDownDodgeDistance = 1.7f;

    float _dodgeDurationInSeconds;

    Vector3 _upDownDirection = Vector3.up;
    Sequence _dodgeTween;


    public void Dodge(Vector3 direction)
    {
        if (IsUpOrDown(direction))
        {

            _upDownDirection = direction;
            _dodgeTween = DOTween.Sequence();
            _dodgeTween.Append(transform.DOLocalMove(direction * _upDownDodgeDistance, _dodgeDurationInSeconds * 0.5f).SetRelative());
            _dodgeTween.Append(transform.DOLocalMove(-direction * _upDownDodgeDistance, _dodgeDurationInSeconds * 0.5f).SetRelative());
        }
    }

    public void PauseDodge()
    {
        if (_dodgeTween !=  null)
        {
            _dodgeTween.Pause();
        }
    }

    public void ResumeDodge()
    {
        if (_dodgeTween != null)
        {
            _dodgeTween.Play();
        }
    }

    public void SetDodgeDurationInSeconds(float duration)
    {
        _dodgeDurationInSeconds = duration;
    }

    bool IsUpOrDown(Vector3 direction)
    {
        return direction == Vector3.up || direction == Vector3.down;
    }
}
