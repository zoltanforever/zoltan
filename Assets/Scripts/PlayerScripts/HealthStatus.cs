﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public delegate void HealthStatusHandler();

public class HealthStatus : MonoBehaviour {

	const float MIN_HEALTH = 0;
	const float MAX_HEALTH = 100;
	const float TIME_NO_DMG_FOR_REGEN = 2;
	const float DURATION_DAMAGE = 0.4f;
	const float MIN_REDNESS = 0.3f;
	const float MAX_REDNESS = 0.9f;
	const float RED_TWEEN_DURATION = 0.2f;

    public event HealthStatusHandler DyingEvent;

    int _healthID;
    int _rednessBlendID;

    Material _bloodMaterial;
	float _healthTarget = 100;
	float _redness = MIN_REDNESS;

	Tween _rednessTween;
	   
#if UNITY_EDITOR
    float _health = 100;
#else
	float _health = 100;
#endif

    SpriteRenderer _renderer;

    float _timeSinceLastDamage;
	float _regenSpeed = 10;

    bool _isDead;

    ZoltanWarriorController _parentController;
    
    void Start() {
        _healthID = Shader.PropertyToID("_Health");
        _rednessBlendID = Shader.PropertyToID("_RednessBlend");

        _renderer = GetComponent<SpriteRenderer>();

        _bloodMaterial = _renderer.material;
        _bloodMaterial.renderQueue = 5000;

        SetHealth(MAX_HEALTH);
        _isDead = false;
        //PositionAndStretchToFitScreen();
    }

    //This does not work
    void PositionAndStretchToFitScreen()
    {
        Camera leftCamera = GameObject.Find("Main Camera Left").GetComponent<Camera>();
        Debug.Log("Darie, Left camera fov" + leftCamera.fieldOfView);

        transform.localPosition = new Vector3(0, 0, 0.4f);
        //On a device, the reported field of view of the camera is 60, even though it actually is 97 or more ...
        float frustumHeight = 2 * transform.localPosition.z * Mathf.Tan(leftCamera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        Debug.Log("Darie, Frustum height is " + frustumHeight);

        float scaleX = frustumHeight * _renderer.sprite.bounds.size.x * leftCamera.aspect;

        float scaleY = frustumHeight * _renderer.sprite.bounds.size.y;
        
   
        transform.localScale = new Vector3(scaleX, scaleY, 1);    
    }

    void Update()
    {
        _timeSinceLastDamage += Time.deltaTime;

		if(_timeSinceLastDamage > TIME_NO_DMG_FOR_REGEN && _health < MAX_HEALTH)
        {
			SetHealth(Mathf.Min(MAX_HEALTH, _health + _regenSpeed * Time.deltaTime));
        }
#if UNITY_EDITOR
		CheckEditorTestKeys();
#endif
    }

    void UpdateState()
    {
		if(_health <= MIN_HEALTH && !_isDead)// && !Application.isEditor)
        {
            if (DyingEvent != null)
            {
                DyingEvent();
            }
            _isDead = true;
            _health = MAX_HEALTH;
        }
    }

    public void TakeDamage(int damage)
    {
        _timeSinceLastDamage = 0;
		_healthTarget = _healthTarget - damage;
		DOTween.To (GetHealth, SetHealth, _healthTarget, DURATION_DAMAGE).SetEase(Ease.OutQuart);	
		SetRedness(MAX_REDNESS);
		_rednessTween.Kill ();
		_rednessTween = DOTween.To (GetRedness, SetRedness, MAX_REDNESS, RED_TWEEN_DURATION).OnComplete(() => {
			DOTween.To(GetRedness, SetRedness, MIN_REDNESS, RED_TWEEN_DURATION);
		});
    }

	void SetHealth(float health)
	{		
		_health = health;
		_bloodMaterial.SetFloat (_healthID, Mathf.Clamp(_health, MIN_HEALTH, MAX_HEALTH) / MAX_HEALTH);
		UpdateState ();
	}

	float GetHealth()
	{
		return _health;
	}

	void SetRedness(float redness)
	{
		_bloodMaterial.SetFloat (_rednessBlendID, redness);
		_redness = redness;
	}

	float GetRedness()
	{
		return _redness;
	}

	void CheckEditorTestKeys()
	{
		if (Input.GetKeyUp(KeyCode.P))
		{
			TakeDamage(20);
		}

		if (Input.GetKeyUp(KeyCode.O))
		{
			_health = MAX_HEALTH;
			_healthTarget = MAX_HEALTH;
		}
	}

}