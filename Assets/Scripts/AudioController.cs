﻿using UnityEngine;
using System.Collections.Generic;

public class AudioController : MonoBehaviour
{
    [SerializeField]
    GameObject _killingSounds;

    public AudioClip _gotDamagedClip;

    List<AudioClip> _killingClips;

    AudioSource _source;

    // Use this for initialization
    void Start()
    {
        _killingClips = _killingSounds.GetComponent<AudioList>().AudioClips;
        _source = GetComponent<AudioSource>();
    }

    public void PlayKillSound()
    {
        _source.PlayOneShot(_killingClips[Random.Range(0, _killingClips.Count)]);
    }

    public void PlayTakeDamageSound()
    {
        _source.PlayOneShot(_gotDamagedClip);
    }
}
