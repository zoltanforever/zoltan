﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRCanvasController : MonoBehaviour {

    public GameObject _canvasPrototype;

    public Camera _leftCamera;
    public Camera _rightCamera;

    Canvas _leftCanvas;
    Canvas _rightCanvas;

	void Start () {
        GameObject canvas = (GameObject)GameObject.Instantiate(_canvasPrototype);

        _leftCanvas = canvas.GetComponent<Canvas>();

        if (_leftCanvas == null)
        {
            Debug.LogError("Canvas prototype on VRCanvas need Canvas component");
            gameObject.SetActive(false);
        }

        _leftCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        _leftCanvas.worldCamera = _leftCamera;

        _rightCanvas = ((GameObject)GameObject.Instantiate(_canvasPrototype)).GetComponent<Canvas>();
        _rightCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        _rightCanvas.worldCamera = _rightCamera;

	}
	
}
