﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    ZoltanWarriorController _zoltanWarriorController;

    // Use this for initialization
    void GoToMenu ()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);

        UpdateHighestScore();
    }

    private void UpdateHighestScore()
    {
        int killCount = _zoltanWarriorController.GetKillCount();
        int currentHighestScore = PlayerPrefs.GetInt(Highscore.NAME);
        if (killCount > currentHighestScore)
        {
            PlayerPrefs.SetInt(Highscore.NAME, killCount);
        }
    }

    public void SetZoltanWarriorController(ZoltanWarriorController controller)
    {
        _zoltanWarriorController = controller;
        _zoltanWarriorController.DeathEvent += GoToMenu;
    }
}
