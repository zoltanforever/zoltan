﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameScript : MonoBehaviour {

    MainMenuItem _item;

    public Highscore HighScorePrefab;

    Highscore _highScore;

    static readonly string MAIN_SCENE_NAME = "Main";

	void Start () {
        _item = GetComponent<MainMenuItem>();
        _item.ItemClicked += LoadMainScene;
        CreateAndUpdateHighScore();
    }

    private void CreateAndUpdateHighScore()
    {
        Highscore _highScore = GameObject.Instantiate(HighScorePrefab);
        _highScore.MyHighscore = PlayerPrefs.GetInt(Highscore.NAME);
    }

    void LoadMainScene()
    {
        SceneManager.LoadScene(MAIN_SCENE_NAME);
    }
}
