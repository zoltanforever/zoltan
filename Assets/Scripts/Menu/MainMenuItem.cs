﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuItem: MonoBehaviour {

    public delegate void MenuItemEventHandler();
    public event MenuItemEventHandler ItemClicked;

    public float _normalScale = 1.0f;
    public float _largeScale = 1.2f;

    GazeInteractableObject _input;

	void Start () {
        _input = GetComponent<GazeInteractableObject>();

        _input.OnEnter += Enlarge;
        _input.OnExit  += Normalize;
        _input.OnClick += Click;
	}

    void Enlarge()
    {
        transform.localScale = new Vector3(_largeScale, _largeScale, 1.0f);
    }

    void Normalize()
    {
        transform.localScale = new Vector3(_normalScale, _normalScale, 1.0f);
    }

    void Click()
    {
        if(ItemClicked != null)
        {
            ItemClicked();
        }
    }

}
