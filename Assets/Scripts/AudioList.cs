﻿using UnityEngine;
using System.Collections.Generic;

public class AudioList : MonoBehaviour {

  [SerializeField]
  private List<AudioClip> _audioClips;

	public List<AudioClip> AudioClips{
    get{ return _audioClips;}
  }
}
