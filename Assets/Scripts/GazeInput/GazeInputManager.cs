﻿using UnityEngine;
using System.Collections;

public class GazeInputManager : MonoBehaviour {

    [SerializeField]
    private Transform _mainCamera;

    [SerializeField]
    private Transform _gazeAim;

    [SerializeField]
    private LayerMask _raycastLayers;

    [SerializeField]
    private float _maxDistance;

    GazeInteractableObject _lastGazed;

    InputManager _inputManager;

    void Start()
    {
        _inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();

        if(_inputManager == null)
        {
            Debug.LogError("Input manager not found in the scene.");
        }
        else
        {
            _inputManager.fireStart += OnClick;
        }
    }

	void OnEnable()
    {
        _lastGazed = null;
    }

    void OnClick()
    {
        if(_lastGazed != null)
        {
            _lastGazed.GazeClick();
        }
    }
	
	void Update () {
        RaycastHit hit;
        if (Physics.Raycast(_mainCamera.position, _gazeAim.forward, out hit, _maxDistance, _raycastLayers))
        {
            GazeInteractableObject gazeHit = hit.collider.GetComponent<GazeInteractableObject>();
            if (gazeHit != null)
            {
                if (_lastGazed != null)
                {
                    if (!gazeHit.Equals(_lastGazed))
                    {
                        _lastGazed.GazeExit();
                        _lastGazed = gazeHit;
                        _lastGazed.GazeEnter();
                    }
                }
                else
                {
                    _lastGazed = gazeHit;
                    _lastGazed.GazeEnter();
                }
            }
        } 
        else if (_lastGazed != null)
        {
            _lastGazed.GazeExit();
            _lastGazed = null;
        }

    }
}
