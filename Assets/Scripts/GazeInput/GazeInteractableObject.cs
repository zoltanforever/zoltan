﻿using UnityEngine;
using System.Collections;

public class GazeInteractableObject : MonoBehaviour {

    public delegate void GazeObjectEventHandler();

    public event GazeObjectEventHandler OnEnter;
    public event GazeObjectEventHandler OnExit;
    public event GazeObjectEventHandler OnClick;

    public void GazeEnter()
    {
        if (OnEnter != null)
            OnEnter();
    }

    public void GazeExit() {
        if (OnExit != null)
            OnExit();
    }

    public void GazeClick()
    {
        if (OnClick != null)
            OnClick();
    }
}
