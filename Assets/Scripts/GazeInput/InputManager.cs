﻿using UnityEngine;
using System.Collections;

public delegate void InputManagerEventHandler();

public class InputManager : MonoBehaviour {

    public event InputManagerEventHandler fireStart;
    public event InputManagerEventHandler fireEnd;

    bool _fireStarted;

	void Start () {
        _fireStarted = false;
	}
	
	void Update () {
        if (Input.GetAxis("Fire1") != 0 && !_fireStarted)
        {
            _fireStarted = true;
            if (fireStart != null)
                fireStart();
        }
        else if (Input.GetAxis("Fire1") == 0 && _fireStarted)
        {
            _fireStarted = false;
            if (fireEnd != null)
                fireEnd();
        }
    }

    
}
