﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    private SpriteRenderer _renderer;

    public int Damage = 1;
    public float LifeTime = 5;

    /// <summary>
    /// Anchor offset for sprite renderer sorting order
    /// </summary>
    public float _anchorOffset;

    AudioSource _audioSource;

    public AudioClip _initialSound;
    public AudioClip _destroyedSound;

    public bool _destroyOnImpact = false;

    public float _destryAfterSeconds;

    Vector3 _cameraPosition;
    Animator _animator;
   
    static int IMPACT_TRIGGER_ID = Animator.StringToHash("Impact");

    bool _gettingDestroyed = false;


    void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _cameraPosition = Camera.main.transform.position;

        Shootable shootable = GetComponent<Shootable>();
        if (shootable)
        {
            shootable.GotShot += OnGetShot;
        }

        _audioSource = GetComponent<AudioSource>();

        if(_audioSource && _initialSound)
        {
            _audioSource.PlayOneShot(_initialSound);
        }

        _animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        _renderer.sortingOrder = -Mathf.RoundToInt(((_cameraPosition - transform.position).magnitude + _anchorOffset) / 0.05f);
        //Billboard
        transform.LookAt(Camera.main.transform, transform.up);

        LifeTime -= Time.deltaTime;
        if (LifeTime <= 0)
        {
            DestroyImmediate();
        }

        _renderer = GetComponent<SpriteRenderer>();

    }

    void OnCollisionEnter(Collision collision)
    {
        DetroyProjectile();
    }

    void OnTriggerEnter(Collider collider)
    {
        DetroyProjectile();
    }
    
    protected virtual void DetroyProjectile()
    {
        if (!_gettingDestroyed)
        {
            _gettingDestroyed = true;

            if (_destroyOnImpact)
            {
                Destroy();
            }

            if (_animator)
            {
                _animator.SetTrigger(IMPACT_TRIGGER_ID);
            }

            if (_audioSource && _destroyedSound)
            {
                _gettingDestroyed = true;
                _audioSource.PlayOneShot(_destroyedSound);
            }
        }
        
    }

    IEnumerator DestroyAfterTimeout()
    {
        yield return new WaitForSeconds(_destryAfterSeconds);
        Destroy(gameObject);
    }

    void Destroy()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        StartCoroutine(DestroyAfterTimeout());
    }

    void DestroyImmediate()
    {
        Destroy(gameObject);
    }

	void OnGetShot(int damage, Transform impact)
    {
        DetroyProjectile();
    }
}
