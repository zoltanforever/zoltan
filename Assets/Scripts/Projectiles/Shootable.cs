﻿using UnityEngine;

public delegate void GotShotEventHandler(int damage, Transform impact);

public class Shootable : MonoBehaviour {

    public event GotShotEventHandler GotShot;

    [SerializeField]
    bool _impactBullet;

	public void GetShot(int damage, Transform impact = null)
    {
		GotShot(damage, impact);
    }

    public bool ImpactBullet
    {
        get { return _impactBullet; }
        set { _impactBullet = value; }
    }
}
