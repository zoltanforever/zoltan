﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barel : MonoBehaviour {

    public float _minAngularVelocity = 10;
    public float _maxAngularVelocity = 300;

	// Use this for initialization
	void Start () {
        int sign = Random.Range(0, 1) < 0.5 ? -1 : 1;
        Vector3 angularVelocity = new Vector3(0, 0, sign * Random.Range(_minAngularVelocity, _maxAngularVelocity));
        GetComponent<Rigidbody>().angularVelocity = angularVelocity;
	}
	
}
