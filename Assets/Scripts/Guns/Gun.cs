﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

    /// <summary>
    /// Bullet that is shot
    /// </summary>
    [SerializeField]
    private GameObject _bullet;

    /// <summary>
    /// Holds remaining bullets
    /// </summary>
    private int _bulletCount = 0;

    /// <summary>
    /// Holds starting bullet count
    /// </summary>
    [SerializeField]
    private int _bulletStartCount;

    /// <summary>
    /// How long it takes for the gun to cool down
    /// </summary>
    [SerializeField]
    private float _shotCooldown;

    /// <summary>
    /// How spread is the shot
    /// </summary>
    [SerializeField]
    private float _spread;

    /// <summary>
    /// How many bullets are created per shot
    /// </summary>
    [SerializeField]
    private int _bulletsPerShot;

    /// <summary>
    /// Damage done by the shot bullet
    /// </summary>
    [SerializeField]
    private int _damage;

    [Tooltip("How many minions does the bullet shoot through")]
    [SerializeField]
    private int _shootThroughNumber;

    /// <summary>
    /// How long to create bullets after the shot
    /// </summary>
    [SerializeField]
    private float _shootTimeOffset;

    /// <summary>
    /// Audio played when the gun is shot
    /// </summary>
    [SerializeField]
    private AudioClip _shootSound;

    /// <summary>
    /// Is the gun shooting projectiles, like bazuka
    /// </summary>
    [SerializeField]
    private bool _shootsProjectile;

    /// <summary>
    /// How much force apply to a projectile
    /// </summary>
    [SerializeField]
    private float _force;

    public float ShotCooldown
    {
        get { return _shotCooldown; }
    }

    public float Spread
    {
        get { return _spread; }
    }

    public int BulletsPerShot
    {
        get { return _bulletsPerShot; }
    }

    public int ShootThroughNumber
    {
        get { return _shootThroughNumber; }
    }

    public int Damage
    {
        get { return _damage; }
    }

    public float ShootTimeOffset
    {
        get { return _shootTimeOffset; }
    }

    public AudioClip ShootSound
    {
        get { return _shootSound; }
    }

    public GameObject BulletPrefab
    {
        get { return _bullet; }
    }

    public bool ShootsProjectile
    {
        get { return _shootsProjectile; }
    }

    public float Force
    {
        get { return _force; }
    }

    public int BulletCount
    {
        get { return _bulletCount; }
        set { _bulletCount = value; }
    }

    public int BulletStartCount
    {
        get { return _bulletStartCount; }
    }
}
