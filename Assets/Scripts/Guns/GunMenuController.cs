﻿
using System.Collections;
using UnityEngine;

class GunMenuController
{
    private const float GUN_MENU_LOOK_TOLERANCE = 90;
    private bool _isGunMenuActive;

    private GameObject _gunMenuGo;

    private GunController _gunController;
    private Transform _warrior;

    public GunMenuController(GunController gunController, Transform warrior, GameObject gunMenuGO)
    {
        _gunController = gunController;
        _warrior = warrior;
        _gunMenuGo = gunMenuGO;
    }

    public void Initialize()
    {
        _gunMenuGo = GameObject.Instantiate(_gunMenuGo);
        _gunMenuGo.SetActive(false);
        _gunMenuGo.GetComponent<GunMenu>().SetGunController(_gunController);
    }

    public void Update()
    {
        if (_isGunMenuActive && IsGazeAwayFromMenu())
        {
            PositionGunMenu();
        }

        if (Input.GetKey(KeyCode.Tab))
        {
            PositionGunMenu();
            SetGunMenuActive(true);
        }
    }

    public void SetGunMenuActive(bool active)
    {
        _gunMenuGo.SetActive(active);
        _isGunMenuActive = active;
    }

    public IEnumerator ActiveGunMenuAfter(float time)
    {
        yield return new WaitForSeconds(time);
        PositionGunMenu();
        SetGunMenuActive(true);
    }

    private bool IsGazeAwayFromMenu()
    {
        float angle = Vector3.Angle(_warrior.forward, _gunMenuGo.transform.forward);
        return angle > GUN_MENU_LOOK_TOLERANCE;
    }

    private void PositionGunMenu()
    {
        _gunMenuGo.transform.rotation = _warrior.rotation;
        _gunMenuGo.transform.position = _warrior.position + new Vector3(0, -0.4f, 0) + _warrior.forward;
    }


}
