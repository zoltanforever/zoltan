﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Guns
{
    public enum GunEnum
    {
        Pistol, Shotgun, DoubleShotgun, Railgun, Bazuka
    }
}
