﻿using UnityEngine;
using System.Collections;

public class GunSelection : MonoBehaviour {

    SpriteRenderer _renderer;

    public int _gunNumber;

    public float _normalScale;
    public float _enlargedScale;

    public Color _normalColor;
    public Color _enlargedColor;

    GazeInteractableObject _gazeInput;
    GunMenu _menu;

	void Start () {
        _renderer = GetComponent<SpriteRenderer>();
        _gazeInput = GetComponent<GazeInteractableObject>();

        _gazeInput.OnEnter += OnEnter;
        _gazeInput.OnExit += OnExit;
        _gazeInput.OnClick += OnClick;

        _menu = transform.parent.GetComponent<GunMenu>();
    }
	
	void OnEnter()
    {
        Enlarge();
    }

    void OnExit()
    {
        Normalize();
    }

    void OnClick()
    {
        _menu.SelectGun(_gunNumber);
    }

    void Normalize()
    {
        transform.localScale = new Vector3(_normalScale, _normalScale, 1);
        _renderer.color = _normalColor;
    }

    void Enlarge()
    {
        transform.localScale = new Vector3(_enlargedScale, _enlargedScale, 1);
        _renderer.color = _enlargedColor;
    }
}
