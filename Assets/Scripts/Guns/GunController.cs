﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Assets.Scripts.Guns;
using System.Linq;

public class GunController : MonoBehaviour {

    public delegate void GunControllerEventHandler();
    public event GunControllerEventHandler GunSelected;

    /// <summary>
    /// List of available guns.
    /// Order must coincide with order in animator
    /// </summary>
    public List<Gun> _guns;

    public int _gunSelection;
    int _pGunSelection;

    Gun _currentGun;

    Animator _animator;
    RaycastHit _hitInfo = new RaycastHit();
    AudioSource _shotSound;

    float _shotTime;

    Transform _nosle;

    public float _gunRange;

    int _minionLayer;

	bool _gunWarmup = true;
	bool _animatorStarted = false;

    InputManager _inputManager;

    bool _paused;

    Transform _camera;

    BulletCountDisplay _bulletCountDisplay;

	Transform _dummyBullet;

    int _animatorShoot;
    int _animatorGun;

    public int GunSelection
    {
        set { _gunSelection = value; }
    }

    public void SetBulletDisplay(BulletCountDisplay bulletDisplay)
    {
        _bulletCountDisplay = bulletDisplay;
    }

    void Start()
    {

        _animatorShoot = Animator.StringToHash("Shoot");
        _animatorGun = Animator.StringToHash("Gun");

        if (_guns == null)
        {
            Debug.Log("You have no guns, stupid!");
            enabled = false;
        }

        for(int i = 0; i < _guns.Count; i++)
        {
            _guns[i].BulletCount = 0;
        }

        _currentGun = _guns[_gunSelection];
        _pGunSelection = _gunSelection;
        SetGun(_pGunSelection);
        _bulletCountDisplay.DisplayBullets(_currentGun.BulletCount);

        _animator = GetComponent<Animator>();
        _shotSound = GetComponent<AudioSource>();

        _animator.SetInteger("Gun", _gunSelection);

        _minionLayer = LayerMask.GetMask("Minion");

        _inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
        _inputManager.fireStart += Fire;
        _inputManager.fireEnd += EndFire;

        _nosle = transform.parent.FindChild("GazeAim");

        _camera = Camera.main.transform;

        _paused = false;

        GameObject go = new GameObject ("Dummy bullet");
        _dummyBullet = go.transform;
    }

    void Update()
    {
        if(_gunSelection != _pGunSelection)
        {
            SetGun(_gunSelection);
        }

        CheckKeyboardShortcuts();
    }

    private void CheckKeyboardShortcuts()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _gunSelection = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _gunSelection = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _gunSelection = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            _gunSelection = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            _gunSelection = 4;
        }
    }

    public void Pause()
    {
        _paused = true;
        EndFire();
    }

    public void Unpause()
    {
        _paused = false;
    }

    void Fire()
    {
        if (!_animatorStarted && !_paused)
        {
            StartAnimation();
        }
    }

    void EndFire()
    {
        if (_animatorStarted)
        {
            _animator.SetBool(_animatorShoot, false);
            _gunWarmup = true;
            _animatorStarted = false;
        }
    }

    public void SetGun(int gunSelection)
    {
        EndFire();

        // Increment bullet count for selected gun
        // if it is not a pistol
        if (gunSelection != (int)GunEnum.Pistol)
        {
            _guns[gunSelection].BulletCount += _guns[gunSelection].BulletStartCount;
        }
        else
        {
            _guns[gunSelection].BulletCount = 999;
        }
        

        if (gunSelection != _pGunSelection)
        {
            _gunSelection = gunSelection;
            _animator.SetInteger(_animatorGun, _gunSelection);
            _currentGun = _guns[_gunSelection];
            _gunWarmup = true;
            _pGunSelection = _gunSelection;
        }

        _bulletCountDisplay.DisplayBullets(_currentGun.BulletCount);

        if (GunSelected != null)
            GunSelected();
    }

    void StartAnimation()
    {
        _animator.SetBool(_animatorShoot, true);
        _animator.SetInteger(_animatorGun, _gunSelection);
        _animatorStarted = true;
    }

    void Shoot()
    {

        _shotSound.PlayOneShot(_currentGun.ShootSound);

        for (int i = 0; i < _currentGun.BulletsPerShot; i++)
        { 
            Vector3 direction = _nosle.forward;
            var randomNumberX = Random.Range(-_currentGun.Spread, _currentGun.Spread);
            var randomNumberY = Random.Range(-_currentGun.Spread, _currentGun.Spread);
            var randomNumberZ = Random.Range(-_currentGun.Spread, _currentGun.Spread);

            direction = Quaternion.Euler(randomNumberX, randomNumberY, randomNumberZ) * direction;

            if (_currentGun.ShootsProjectile)
            {
				GameObject bullet = (GameObject)Instantiate(_currentGun.BulletPrefab, _camera.position, _nosle.rotation);
				bullet.transform.Rotate(randomNumberX, randomNumberY, randomNumberZ);
                bullet.GetComponent<Rigidbody>().AddForce(transform.forward * _currentGun.Force);
            }
            else {
                RaycastHit[] hits = Physics.RaycastAll(_camera.position, direction, _gunRange, _minionLayer).OrderBy(x => x.distance).ToArray();
                if (hits.Length > 0)
				{
                    int count = 0;
                    int monstersHit = 0;

                    while (monstersHit <= _currentGun.ShootThroughNumber && count < hits.Length)
                    {
                        Shootable shot = hits[count].transform.GetComponent<Shootable>();
                        _dummyBullet.position = hits[count].point;
                        _dummyBullet.rotation = _nosle.rotation;
                        _dummyBullet.Rotate(Vector3.up, 180);
                        shot.GetShot(_currentGun.Damage, _dummyBullet);
                        if (shot.ImpactBullet)
                        {
                            monstersHit++;
                        }

                        count++;
                    }
                }
            }
        }

        if (_pGunSelection != (int)GunEnum.Pistol)
        {
            // Don't reduce bullet count for pistol
            _currentGun.BulletCount -= 1;
        }

        _bulletCountDisplay.DisplayBullets(_currentGun.BulletCount);

        if(_currentGun.BulletCount == 0)
        {
            // Revert gun to pistol if out of bullets
            _gunSelection = (int)GunEnum.Pistol;
        }
    }
}
