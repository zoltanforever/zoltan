﻿using UnityEngine;
using System.Collections;

public class GunMenu : MonoBehaviour {

    GunController _gunController;

    public void SetGunController(GunController gunController)
    {
        _gunController = gunController;
    }

    public void SelectGun(int gunNumber)
    {
        _gunController.SetGun(gunNumber);
    }
}
