﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public delegate void MinionSpawnerEventHandler(BaseMinion minion);

public class ZoltonMinionSpawner : MonoBehaviour {

    public event MinionSpawnerEventHandler MinionSpawned;
    
    public float _spawnEverySeconds = 0.5f;
    public int _spawnDistance;
    public float _angleOfSpawn;

    protected List<MinionModel> _minions = new List<MinionModel>();

    Transform _cameraTransform;

    float _spawnTimeCountdown;
    private bool _spawnStarted;
    
	void Start () {
        _cameraTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    public void SetMinions(List<MinionModel> minionsToSpawn)
    {
        _minions.Clear();
        _minions = minionsToSpawn;
    }

    public virtual void StartSpawning()
    {
        _spawnTimeCountdown = 0;
        _spawnStarted = true;
    }

    public void StopSpawning()
    {
        _spawnStarted = false;
    }

    protected GameObject InstantiateRandomMonster()
    {
        GameObject nextMinion = PickRandomMonster(_minions).MinionGo;
        return InstantiateMonster(nextMinion);
    }

    protected GameObject InstantiateMonster(GameObject monster)
    {
        float angle = Random.Range(0, _angleOfSpawn);
        Vector3 forward = transform.forward;
        Vector3 spawnPos = Quaternion.Euler(0, angle, 0) * transform.forward * _spawnDistance;
        spawnPos.y = monster.transform.position.y;

        GameObject minion = (GameObject)GameObject.Instantiate(monster, spawnPos, _cameraTransform.rotation);
        minion.SetActive(true);

        if (MinionSpawned != null)
        {
            MinionSpawned(minion.GetComponentInChildren<BaseMinion>());
        }

        return minion;
    }
	
	protected void Update () {
	    if(_spawnStarted)
        {
            _spawnTimeCountdown -= Time.deltaTime;
            if (_spawnTimeCountdown <= 0)
            {
                InstantiateRandomMonster();
                _spawnTimeCountdown = _spawnEverySeconds;
            }
        }
	}

    protected MinionModel PickRandomMonster(List<MinionModel> monsters)
    {
        int sum = monsters.Sum(m => m.Weight);
        int randomPick = Random.Range(1, sum + 1);
        int index = 0;

        randomPick -= monsters[index].Weight;
        while (randomPick > 0)
        {
            index++;
            randomPick -= monsters[index].Weight;
        }
        return monsters[index];
    }


    protected void OnDrawGizmos(){
        Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position, transform.forward * _spawnDistance);
        Vector3 tmp = Quaternion.Euler(0, _angleOfSpawn, 0) * transform.forward;
        Gizmos.DrawLine(transform.position, tmp * _spawnDistance);
    }
}
