﻿using UnityEngine;

public class ZoltonMinibosSpawner : ZoltonMinionSpawner
{
    public override void StartSpawning()
    {
        GameObject miniBos = PickRandomMonster(_minions).MinibosGo;
        InstantiateMonster(miniBos);
        base.StartSpawning();
    }
}
