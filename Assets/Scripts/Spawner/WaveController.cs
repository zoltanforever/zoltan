﻿using System;
using System.Collections.Generic;
using UnityEngine;

public delegate void WaveControllerEventHandler();

public class WaveController : MonoBehaviour
{
    public event WaveControllerEventHandler StageEnded;
    public event WaveControllerEventHandler AllStagesEnded;


    public List<MinionSpawnStage> _spawnStages;

    private MinionSpawnStage _currentStage;
    private int _currentStageIndex;
    private int _minionsSpawned;
    private int _currentMinionCap;

    int _totalMinionsLeft;
    
    [Serializable]
    public class MinionSpawnStage
    {
        public ZoltonMinionSpawner Spawner;
        public int MinionCap;
        public bool HasStarted;
    }

    void Awake()
    {
        foreach(MinionSpawnStage spawner in _spawnStages)
        {
            spawner.Spawner.MinionSpawned += IncrementMinionCounter;
        }

        _currentStageIndex = 0;
    }

    public void SetMinions(List<MinionModel> minions)
    {
        foreach (MinionSpawnStage spawner in _spawnStages)
        {
            spawner.Spawner.SetMinions(minions);
        }
    }

    public void StartNextStage()
    {
        if (_spawnStages.Count > _currentStageIndex)
        {
            _minionsSpawned = 0;
            _currentStage = _spawnStages[_currentStageIndex];
            _currentStage.HasStarted = true;
            _currentStage.Spawner.StartSpawning();
            _totalMinionsLeft = _currentStage.MinionCap;
            _currentMinionCap = _currentStage.MinionCap;
            _currentStageIndex++;
        }
        else
        {
            if (AllStagesEnded != null)
            {
                AllStagesEnded();
            }
        }
    }
	
    void IncrementMinionCounter(BaseMinion minion)
    {
        _minionsSpawned++;

        if (_minionsSpawned == _currentMinionCap)
        {
            _currentStage.Spawner.StopSpawning();
        }

        minion.DieEvent += DecrementTotalMinions;
    }

    void DecrementTotalMinions()
    {
        _totalMinionsLeft--;
    }
    
    void Update()
    {
        if (_currentStage != null)
        {
            if (_currentStage.HasStarted && _totalMinionsLeft == 0)
            {
                EndCurrentStage();
            }
        }
    }

    private void EndCurrentStage()
    {
        _currentStage.HasStarted = false;
        if (StageEnded != null)
        {
            StageEnded();
        }
    }
}
