﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public delegate void AutoSpawnerEvent();

public class AutoSpawnerController : MonoBehaviour
{
    public event AutoSpawnerEvent WaveEnded;
    public event AutoSpawnerEvent WaveStageEnded;

    public bool IsDebugMode;

    [SerializeField]
    private int _startDifficulty;

    [SerializeField]
    private int _difficultyIncrease;

    [SerializeField]
    private List<GameObject> _minionObjects;
    [SerializeField]
    private List<GameObject> _minibosObjects;

    [SerializeField]
    private List<WaveController> _waveControllers;

    private int _currentWaveIndex;

    private int _currentDifficulty;
    private List<MinionModel> _minions;
    private List<GameObject> _currentMinions;
    
	public void Initialize() {
        if (_minionObjects == null || _minibosObjects == null)
        {
            Debug.LogError("No minion assigned to AutoMinionSpawner.");
        }

        _currentDifficulty = _startDifficulty;

        InitializeMinionsList();

        InitializeWaveControllers();
    }


    private void InitializeWaveControllers()
    {
        _currentWaveIndex = 0;
        AttatchWaveControllerEvents();
    }

    private void AttatchWaveControllerEvents()
    {
        CurrentWaveController.StageEnded += OnStageEnded;
        CurrentWaveController.AllStagesEnded += OnAllStagesEnded;
    }

    private void RemoveWaveControllerEvents()
    {
        CurrentWaveController.StageEnded -= OnStageEnded;
        CurrentWaveController.AllStagesEnded -= OnAllStagesEnded;
    }

    private void OnStageEnded()
    {
        if (WaveStageEnded != null)
        {
            WaveStageEnded();
        }

        CurrentWaveController.StartNextStage();
    }

    private void OnAllStagesEnded()
    {
        if (WaveEnded != null)
        {
            WaveEnded();
        }

        StartNextWave();
    }

    private void StartNextWave()
    {
        RemoveWaveControllerEvents();
        _currentWaveIndex = (_currentWaveIndex + 1) % _waveControllers.Count;
        AttatchWaveControllerEvents();
        _currentDifficulty += _difficultyIncrease;
        StartWaves();
    }

    public void StartWaves()
    {
        CurrentWaveController.SetMinions(GetMinionsForDifficulty(_currentDifficulty));
        CurrentWaveController.StartNextStage();
    }

    private WaveController CurrentWaveController
    {
        get
        {
            return _waveControllers[_currentWaveIndex];
        }
    }
	
    private void InitializeMinionsList()
    {
        _minions = new List<MinionModel>();

        for(int i = 0; i < _minionObjects.Count; i++) {
            GameObject minion = _minionObjects[i];
            BaseMinion baseMinion = minion.GetComponentInChildren<BaseMinion>();
            _minions.Add(new MinionModel(baseMinion.MinionDifficulty, minion, _minibosObjects[i]));
        }
    }

    private List<MinionModel> GetMinionsForDifficulty(int difficulty)
    {
        List<MinionModel> result = new List<MinionModel>();
        int difficultPointsLeft = difficulty;

        while(difficultPointsLeft > 0)
        {
            MinionModel selectedMinion = PickRandomMonster(FilterMinionsByDifficulty(_minions, difficultPointsLeft));
            difficultPointsLeft -= selectedMinion.Difficulty;
            MinionModel possibleCopy = result.Find(m => m.MinionGo == selectedMinion.MinionGo);
            if (possibleCopy == null)
            {
                result.Add(selectedMinion);
            }
            else
            {
                possibleCopy.Weight += 1;
            }
        }

        LogMonsters(result);

        return result;
    }

    private List<MinionModel> FilterMinionsByDifficulty(List<MinionModel> minions, int difficulty)
    {
        return minions.FindAll(m => m.Difficulty <= difficulty);
    }

    private MinionModel PickRandomMonster(List<MinionModel> monsters)
    {
        int sumOfDifficulties = monsters.Sum(m => m.Difficulty);
        int randomPick = Random.Range(1, sumOfDifficulties+1);
        int index = 0;

        randomPick -= monsters[index].Difficulty;
        while (randomPick > 0)
        {
            index++;
            randomPick -= monsters[index].Difficulty;
        }
        return monsters[index];
    }

    private void LogMonsters(List<MinionModel> list)
    {
        Log("Monsters in the list are: ");
        int difficultySum = 0;
        foreach(MinionModel minion in list)
        {
            Log("Minion" + minion.MinionGo.name + " difficulty: " + minion.Difficulty + " weight " + minion.Weight);
            difficultySum += minion.Difficulty;
        }

        Log("Total is " + difficultySum + " and was supposed to be " + _currentDifficulty);
    }

    private void Log(string text)
    {
        if (IsDebugMode)
        {
            Debug.Log(text);
        }
    }


#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            StartWaves();
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            if (WaveStageEnded != null)
            {
                WaveStageEnded();
            }
        }
    }
#endif
}

public class MinionModel
{
    public int Difficulty;
    public int Weight;
    public GameObject MinionGo;
    public GameObject MinibosGo;

    public MinionModel(int difficulty, GameObject minionGo, GameObject minibosGo)
    {
        Difficulty = difficulty;
        MinionGo = minionGo;
        MinibosGo = minibosGo;
        Weight = 1;
    }
}
