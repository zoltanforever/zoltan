﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionSounds : MonoBehaviour {

    public AudioClip _spawningSound;
    public AudioClip _getDamagedSound;
    public AudioClip _attackingSound;
    public AudioClip _deathSound;

    AudioSource _audioSource;

    BaseMinion _minion;

    const float PLAY_DELAY = 0.2f;

    void Start ()
    {
        _minion = GetComponentInChildren<BaseMinion>();
        _audioSource = GetComponent<AudioSource>();

        _minion.BornEvent += PlaySpawnSound;
        _minion.AttackEvent += PlayAttackSound;
        _minion.GotDamagedEvent += PlayGetDamagedSound;
        _minion.DyingEvent += PlayDeathSound;
	}

    void PlaySpawnSound()
    {
        PlayAudioClip(_spawningSound);
    }

    void PlayGetDamagedSound()
    {
        PlayAudioClip(_getDamagedSound, delay: PLAY_DELAY);
    }

    void PlayAttackSound()
    {
        PlayAudioClip(_attackingSound);
    }

    void PlayDeathSound()
    {
        PlayAudioClip(_deathSound, delay: PLAY_DELAY, stopOthers: true);
    }

    void PlayAudioClip(AudioClip clip, float delay = 0, bool stopOthers = false)
    {
        if (clip != null)
        {
            if (stopOthers)
            {
                _audioSource.Stop();
                
            }
            _audioSource.clip = clip;
            _audioSource.PlayDelayed(delay);
        }
    }
}
