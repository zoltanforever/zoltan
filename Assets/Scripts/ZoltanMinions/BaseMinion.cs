﻿using UnityEngine;


public delegate void MinionEventHandler();

public class BaseMinion : MonoBehaviour
{
    static int IS_MOVING_BOOL_ID = Animator.StringToHash("IsMoving");
    static int GET_SHOT_TRIGGER_ID = Animator.StringToHash("GetShot");
    static int ATTACK_TRIGGER_ID = Animator.StringToHash("Attack");
    static int DIE_TRIGGER_ID = Animator.StringToHash("Die");

    public int Health = 1;
    public int Damage = 10;
    public float AttackCooldown = 1f;
    public float AttackRange = 2f;
    public int MinionDifficulty;

    public GameObject BloodEffect;
    public float ZOffset;

    public MinionEventHandler BornEvent;
    public MinionEventHandler AttackEvent;
    public MinionEventHandler GotDamagedEvent;
    public MinionEventHandler DieEvent;
    public MinionEventHandler DyingEvent;

    protected GameObject _mainCamera;
    protected ZoltanWarriorController _player;
    protected Animator _animator;

    float _lastAttackTime;
    bool _isDead = false;
    bool _isMoving = false;

    SpriteRenderer _renderer;
    Vector3 _cameraPosition;

    void Start()
    {
        _mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        _player = _mainCamera.GetComponentInChildren<ZoltanWarriorController>();
        Initialize();

        _renderer = GetComponent<SpriteRenderer>();
        _cameraPosition = Camera.main.transform.position;
        
        Shootable shootable = gameObject.AddComponent<Shootable>();
        shootable.ImpactBullet = true;
        shootable.GotShot += OnGotShot;

        _animator = GetComponent<Animator>();

        if (BornEvent != null)
        {
            BornEvent();
        }
    }

    virtual protected void Initialize()
    {

    }
		
    void Update()
    {
        _renderer.sortingOrder = -Mathf.RoundToInt(((_cameraPosition - transform.position).magnitude + ZOffset) / 0.05f);
		  
        float distanceToPlayer = (_mainCamera.transform.position - transform.position).magnitude;
        if (distanceToPlayer <= AttackRange)
        {
            if (Time.time - _lastAttackTime > AttackCooldown)
            {
                AttackAnimation();
                _lastAttackTime = Time.time;
            }
        }       
    }

    public bool isMoving
    {
        get { return _isMoving; }
        set {
            if (!_isDead)
            {
                _isMoving = value;
                _animator.SetBool(IS_MOVING_BOOL_ID, _isMoving);
            }
        }
    }

    virtual protected void OnCollisionEnter(Collision c) {
        Projectile projectile = c.collider.GetComponent<Projectile>();
		if (projectile)
        {
			OnGotShot(projectile.Damage, projectile.transform);
        }
    }

    virtual protected void OnTriggerEnter(Collider c)
    {
        Projectile projectile = c.GetComponent<Projectile>();
        if (projectile)
        {
            OnGotShot(projectile.Damage, projectile.transform);
        }
    }

	virtual public void OnGotShot(int damage, Transform impact)
    {
        Health -= damage;
        _animator.SetTrigger(GET_SHOT_TRIGGER_ID);

        if (GotDamagedEvent != null)
        {
            GotDamagedEvent();
        }

        if (Health <= 0)
        {
			Die(impact);
        }     
    }

    virtual protected void AttackAnimation()
    {
        if (!_isDead)
        {
            if (AttackEvent != null)
            {
                AttackEvent();
            }
			_animator.SetTrigger(ATTACK_TRIGGER_ID);         
        }
    }

	//This should be called by an animation!
	protected void DamagePlayer()
	{
		_player.TakeDamage(Damage, transform);
	}

	void Die(Transform impact)
    {
        if (!_isDead)
        {
            _isDead = true;
			Instantiate(BloodEffect, impact.position, impact.rotation);
            _mainCamera.GetComponent<ZoltanWarriorController>().IncrementKillCount();     
            _animator.SetTrigger(DIE_TRIGGER_ID);
            if (DyingEvent != null)
            {
                DyingEvent();
            }
        }
    }

    void Destroy()
    {
        if (DieEvent != null)
        {
            DieEvent();
        }
    }
}
