﻿using System;
using UnityEngine;

[RequireComponent(typeof(BaseMinion))]
public class MiniBoss : MonoBehaviour
{
    BaseMinion _minion;

    public float HealthMultiplier = 5;
    public float ScaleMultiplier = 3;
    public float DamageMultiplier = 2;

    // Use this for initialization
    void Start()
    {
        _minion = GetComponent<BaseMinion>();
        _minion.Health = (int) (_minion.Health *  HealthMultiplier);
        _minion.transform.localScale *= ScaleMultiplier;
        _minion.Damage = (int) (_minion.Damage * DamageMultiplier);
    }
}
