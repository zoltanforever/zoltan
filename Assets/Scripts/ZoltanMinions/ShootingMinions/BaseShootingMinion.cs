﻿using UnityEngine;
using System.Collections;

public class BaseShootingMinion : BaseMinion {

    public GameObject _projectilePrefab;
    public GameObject _projectileSpawnPoint;

    protected int _force = 200;

    protected float SHOOTING_HEIGHT = 2.5f;

	//This should be called from the animation!
    virtual protected void Shoot()
    {
        GameObject projectile = CreateProjectile();

        Vector3 target = _mainCamera.transform.position;
        target.y = SHOOTING_HEIGHT;
        Vector3 direction = target - _projectileSpawnPoint.transform.position;

        projectile.GetComponent<Rigidbody>().AddForce(direction.normalized * _force);
    }

    protected GameObject CreateProjectile()
    {
        GameObject projectile = (GameObject)Instantiate(_projectilePrefab, _projectileSpawnPoint.transform.position, _projectileSpawnPoint.transform.rotation);
        Projectile p = projectile.GetComponent<Projectile>();
        if (p == null)
        {
            Debug.LogError("Projectile prefab should have the Projectile component attached!");
        }
        projectile.GetComponent<Projectile>().Damage = Damage;
        return projectile;
    }
}
