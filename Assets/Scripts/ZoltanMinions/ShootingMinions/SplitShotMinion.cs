﻿using UnityEngine;
using System.Collections;

public class SplitShotMinion : BaseShootingMinion {

    int _nrProjectiles = 8;
    float _fanAngle = 30f;

    protected override void Shoot()
    {
        Vector3 target = _mainCamera.transform.position;
        target.y = SHOOTING_HEIGHT;

        float currentAngle = -_fanAngle * 0.5f;
        float angleIncrease = _fanAngle / _nrProjectiles;
        Vector3 direction = target - transform.position;
        for (int i = 0; i < _nrProjectiles; i++)
        {
            Vector3 bulletDirection = Quaternion.Euler(0, currentAngle, 0) * direction;
            currentAngle += angleIncrease;
            GameObject projectile = CreateProjectile();
            projectile.GetComponent<Rigidbody>().AddForce(bulletDirection.normalized * _force);
        }
    }

}
