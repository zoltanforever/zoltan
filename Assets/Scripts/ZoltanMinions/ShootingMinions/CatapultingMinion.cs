﻿using UnityEngine;

public class CatapultingMinion : BaseShootingMinion {

    float _bulletMass;

    float _minForce = 12f;
    float _maxForce = 13f;

    Vector3 bulletDirection;

    protected override void Initialize()
    {
        base.Initialize();
        _bulletMass = _projectilePrefab.GetComponent<Rigidbody>().mass;
    }

    protected override void Shoot()
    {
        GameObject bullet = CreateProjectile();

        Vector3 target = _mainCamera.transform.position;
        target.y = SHOOTING_HEIGHT;

        float force = Random.Range(_minForce, _maxForce);
        float angle = ComputeAngleSoWeHitPlayer(force);

        bulletDirection = Vector3.RotateTowards((target - transform.position).normalized, transform.up, angle, 1);

        bullet.GetComponent<Rigidbody>().AddForce(bulletDirection.normalized * force, ForceMode.Impulse);
    }


    float ComputeAngleSoWeHitPlayer(float forceMagnitude)
    {
        float v = forceMagnitude / _bulletMass;
        float g = Physics.gravity.magnitude;
		//+1 is a hack. Need to aim slightly behind the player to hit the player, because the player collider is above ground.
        float dist = Mathf.Abs((_mainCamera.transform.position - transform.position).magnitude) + 1;
        return Mathf.Atan2(v * v + Mathf.Sqrt(v * v * v * v - g * g * dist * dist), g * dist);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + bulletDirection * 3);
    }

}
